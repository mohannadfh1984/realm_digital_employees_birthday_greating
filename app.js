const axios = require('axios');
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'youremail@gmail.com',
    pass: 'yourpassword'
  }
});
const _senderEmail = "youremail@gmail.com";
const _basedDomain="@realmdigital.co.za";
const _url = "https://interview-assessment-1.realmdigital.co.za/employees";

var _sarvedDate = null;
var _date = new Date();

console.log(`Realm Digital Birthday Greating Service Start ! `);

startService = async () => {
    var employees = [];
    _date.setTime(Date.now());
    if (!_sarvedDate) {
        employees = await  getEmployees();
        checkBirthdayEmployees(employees , _date);
        _sarvedDate=_date;
       
    }
        setInterval(async () => {
            if (itsNewDay(_date)) {
                employees = await  getEmployees();
                checkBirthdayEmployees(employees ,_date);
                _sarvedDate=_date;
            }
        }, 3600 * 1000);
    
}
const getEmployees = async () => {
    try {           
        const resp = await axios.get(_url  );        
        return resp.data;
    } catch (err) {
        // Handle Error Here
        console.error(err);
        return [];
    }
};
const  itsNewDay = (date) => {
    return _sarvedDate.getYear() != _date.getYear() || _sarvedDate.getMonth() != _date.getMonth() || _sarvedDate.getDay() != _date.getDay()
}
const checkBirthdayEmployees=(employees , date)=>{
    
    employees.forEach(async (item)=>{
        e_Date = new Date (item.dateOfBirth); 
        var didntReaciveThisYear =true;
        if(item.lastNotification!=null)  {    
            didntReaciveThisYear =  new Date(item.lastNotification).getYear() != _date.getYear()
        } 
        // he/she born in 29/2
        if(e_Date.getMonth()==1 && e_Date.getDate()==30)           
        {
                if(date.getDate() ==29 && date.getMonth()==1 &&  item.employmentEndDate==null && didntReaciveThisYear && item.employmentStartDate ){
                    await sendGreading(item);
                    return;
                }
        }
        //end  he/she born in 29/2  


        if(e_Date.getMonth() ==date.getMonth() && e_Date.getDate() ==date.getDate()  &&  item.employmentEndDate==null && item.employmentStartDate&&didntReaciveThisYear   ){
          await sendGreading(item)
        }
    })
}
const sendGreading= async (employee)=>{
    mailOptions={
        from: _emailsender,
        to:     (employee.name[0]+employee.lastname + _basedDomain).toLowerCase(),
        subject: `Happy Birthday !!!! ${employee.name} ${employee.lastname}`,
        text: `Happy Birthday !!!! ${employee.name} ${employee.lastname}`
    }
    transporter.sendMail(mailOptions,async (error, info)=>{
        if (error) {
          console.log(error);
        } else {
            console.log(`Greating sent to ${employee.name} ${employee.lastname}`)
          await updateEmployeeRecivedDate(employee)
        }
      });
}
const updateEmployeeRecivedDate= async(employee)=>{
    try {           
        employee.lastNotification=_date.toShortDateString();
        await axios.put(_url+"/"+employee.id ,employee   );                
    } catch (err) {
        // Handle Error Here
        console.error(err);       
    }
}
startService();